const { Mongoose } = require("mongoose");

var Schema = Mongoose.Schema;

let noteSchema = new Schema({
    note: String,
    titre: String,
    priority: String,
    author: String
},{
    timestamps:true
});

module.export = mongoose.model("Notes", noteSchema);

