class Note{
    // besoin de quoi dans la note? d'un id, de la note, priority, author
    constructor(id, note, priority, author, titre){
            this.id = id; 
            this.note = note;
            this.priority = priority;
            this.author = author;
            this.titre = titre;
    }
}

const notes = [
    new Note(0, "blabla woof woof", "urgent", "Allycia P", "titre 0"),
    new Note(1, "blabla woof woof", "urgent", "Paul P", "titre 1"),
    new Note(2, "blabla woof woof", "urgent", "Felix D", "titre 2"),
    new Note(3, "blabla woof woof", "urgent", "Laurie B", "titre 3")
]

exports.addNote = function(data){
    let id= notes.length;
    notes.push(new Note(id, data.note, data.priority, data.author, data.title));
    return true;
}

exports.getNoteById = function(id){
    return notes[id]
}

exports.allNotes = notes;