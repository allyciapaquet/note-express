exports.index = function(req, res, next){
    res.render('notes/index', {
        title: 'Notes',
        notes: notes.allNotes
    })
}

exports.add = function(req, res, next){
    res.render("notes/addnote");
}

exports.addNote = function(req, res, next){
    notes.addNote(req.body);
    res.redirect('notes/');
}

exports.getnotebyId = function(req, res, next){
    res.render('notes/note', {note:notes.getNoteById(req.params.id)});
}