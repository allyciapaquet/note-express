var express = require('express');
var router = express.Router();
var notes = require('../modules/note');
var noteController = require("../controllers/noteController");

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Notes' });
// });
router.get('/', noteController.index);

router.get('/note/add', noteController.add);

router.post('/note/add',  noteController.addNote);

router.get('/note/:id', noteController.getnotebyId);

module.exports = router;
